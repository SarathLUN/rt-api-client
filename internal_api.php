<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Intenal API</title>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
</head>
<body>
<?php
$base_url = "http://192.168.160.251/" ;
//$base_url = "http://127.0.0.1:8000/";
$token_code = "eyJpdiI6InVsN0JQVFgrYm9OZjBkTWliTkRJV3c9PSIsInZhbHVlIjoiMHdtd1V3c2M5OVFlOUoxODZrenIyc1VLalZvRUtxeE5mYzByT0NNeTE4OD0iLCJtYWMiOiI1MmRjYzI0MjkxMWVmZTBlMWE4YWMxZDZiYjg4ZTIxNzU1MGYxZWE3NTBmMjQyMjk4ZjFhZWNiMDk5MzgxYmQ1In0=";
?>
<div class="container" style="margin-top: 100px">
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <div class="col-sm-12">

                    <input type="number" class="form-control" id="customer_id"
                           placeholder="Enter Customer ID" required> <br/>
                    <input type="submit" class="btn-success" id="btn_get_sid" value="Customer Information and Services"></input>  <!-- Image loader -->

                    <img src='loader.gif' id="loader" width='40px' height='40px' style="display: none">

                    <br/><br/>
                </div>
                <div class="col-sm-12">
                    <table id="customer_info"  class="table">
                        <tr><th>Customer Name</th><th>Occupation</th></tr>
                    </table> <br/>


                    <table id="contact_info"  class="table">
                        <tr>
                            <th>Contact Name</th>
                            <th>Phone</th>
                        </tr>
                    </table>

                    <br/>
                    <h5 for="Service">Services</h5>
                    <select class="js-example-basic-single" id="service" style="width: 100%;">
                        <option>Please Select</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-striped">
                <tr>
                    <th scope="col">Service Name</th>
                    <th scope="col">Monthly Fee</th>
                    <th scope="col">Service SLA</th>
                    <th scope="col">Plan</th>
                    <th scope="col">Product</th>
                    <th scope="col">Region</th>
                    <th scope="col">Location</th>
                </tr>
                <tr id="service_detail">

                </tr>
            </table>
        </div>
    </div>


</body>
</html>

<script>
    $(document).ready(function () {
        $('.js-example-basic-single').select2();
        $("#btn_get_sid").click(function () {
            var customer_id = $("#customer_id").val();
            var options = '';

            $.ajax({
                url: '<?php echo $base_url ?>' + 'api/get-customer',
                type: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'app-token': '<?php echo $token_code ?> ',

                },
                contentType: 'application/json',
                dataType: 'json',

                data: JSON.stringify(
                    {"customer_id": customer_id}
                ),
                beforeSend: function () {
                    $("#loader").show();
                },
                success: function (data, textStatus, xhr) {
                    // check customer if still exits
                    if (data['data'].length == 0 ) {
                        alert("customer not found");
                        $('#service').empty();

                        $('#customer_info').empty();

                        $('#contact_info').empty();

                        $('#service_detail').empty();
                    } else {
                        // if exits customer find the service

                        // add customer information
                        var customer_data = data['data'][0] ;
                        var customer_info = '' ;
                        customer_info = "<tr>" +
                            "<th>Customer Name </th> " +
                            "<th>Occupation </th>" +
                            "</tr>";
                        customer_info += "<tr><td>"+customer_data['customer_name'] +"</td>" ;
                        customer_info += "<td>"+customer_data['occupation'] +"</td></tr>" ;
                        $('#customer_info').empty()
                            .append(customer_info);

                        // add to contact table
                        var contact_data = data['data'][0]['contact'] ;
                        var contact_info = '' ;
                        contact_info += "<tr>" +
                            "<th>Contact Name </th> " +
                            "<th>Contact Phone </th>" +
                            "</tr>";
                        if (contact_data.length > 0) {

                            for (var i = 0; i < contact_data.length; i++) {
                                contact_info += "<tr><td>" + contact_data[i]['name'] + "</td>";
                                contact_info += "<td>" + contact_data[i]['phone'] + "</td></tr>";
                            }
                            $("#contact_info").empty().append(contact_info);
                        }


                        var datas = data['data'][0]['service'];
                        if (datas.length > 0) {
                            for (var i = 0; i < datas.length; i++) {
                                options += "<option value='" + datas[i]['service_id'] + "'>" + datas[i]['service_id'] + " - " + datas[i]['service_name'] + "</option>";
                            }
                            $('#service').empty()
                                .empty()
                                .append("<option>Please Select</option>" + options);



                            $("#service_detail").empty().append("<tr><td colspan='2'></td></tr>");
                        } else {
                            alert("Service Not Found ");

                            $('#service').empty()
                                .empty()
                                .append("<option>Please Select</option>");


                            $('#customer_info').empty()
                                .empty();

                            $("#service_detail").empty().append("<tr><td colspan='2'></td></tr>");
                        }

                    }
                    $("#loader").hide();
                },
                error: function (xhr, textStatus, errorThrown) {
                    $("#loader").hide();

                    console.log(xhr.responseText);
                }
            });
        });

        $("#service").change(function () {
            var service_id = $("#service").val();
            var tr = "";
            $.ajax({
                url: '<?php echo $base_url ?>' + 'api/get-service-detail',
                type: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'app-token': '<?php echo $token_code ?>',

                },
                contentType: 'application/json',
                dataType: 'json',

                data: JSON.stringify(
                    {"service_id": service_id}
                ),
                success: function (data, textStatus, xhr) {
                    console.log(data);
                    if (data['status'] == true) {
                        tr ="</td>" + "<td>" + data['service_name'] + "</td>" +
                            "<td>"  + "$ "   + data['monthly_fee'] +
                            "</td>" + "<td>" + data['service_sla'] + "</td>" +
                            "<td>" + data['plan'] + "</td>" +
                            "<td>" + data['product'] + "</td>" +
                            "<td>" + data['region'] + "</td>"+
                            "<td>" + data['location'] + "</td>";
                    } else {
                        tr = "<td colspan='2'>Data Not Found</td>";
                    }
                    $("#service_detail").empty().append(tr);
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log(xhr.responseText);
                }
            });
        });
    });
</script>